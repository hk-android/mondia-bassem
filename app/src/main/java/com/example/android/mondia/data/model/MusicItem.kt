package com.example.android.mondia.data

import org.json.JSONObject

data class MusicItem(val json: String = "{}") : JSONObject(json) {

    val id = optInt("id")
    val image = optJSONObject("cover")?.optString("default")
    val title = optString("title")
    val type = optString("type")
    val artist = optJSONObject("mainArtist")?.optString("name")
    val publishingDate = optString("publishingDate")

}
