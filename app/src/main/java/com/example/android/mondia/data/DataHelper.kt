package com.example.android.mondia.data

import org.json.JSONArray
import org.json.JSONObject

enum class ApiStatus { LOADING, ERROR, DONE }

object DataHelper : DataInterface {

    private var token: String? = null

    override suspend fun search(query: String): Result<List<MusicItem>> = Result {
        when (val result = ApiHelper.search(token ?: getToken(), query)) {
            is Result.Success -> result.value.getSearchResults()
            is Result.Failure -> {
                if (result.cause is UnAuthorizedException) {
                    token = null
                    search(query).requireValue()
                } else
                    throw result.cause
            }
        }
    }

    private suspend fun getToken(): String {
        val result = ApiHelper.getToken().requireValue()
        return result.getTokenString().also { token = it }
    }
}

private fun String.getSearchResults(): List<MusicItem> = with(JSONArray(this))
{
    return 0.until(this.length()).map { i -> optJSONObject(i) }
        .map { MusicItem(it.toString()) }
}

private fun String.getTokenString(): String = JSONObject(this).optString("accessToken")


