package com.example.android.mondia.data


interface DataInterface {

    suspend fun search(query: String): Result<List<MusicItem>>

}