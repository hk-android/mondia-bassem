/*
 * Copyright (C) 2021 The Android Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.mondia.view

import android.widget.SearchView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.mondia.data.MusicItem
import com.example.android.mondia.data.ApiStatus
import com.example.android.mondia.data.DataHelper
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import com.example.android.mondia.data.Result

class MusicSharedViewModel : ViewModel(), SearchView.OnQueryTextListener {

    private val _status = MutableStateFlow(ApiStatus.DONE)
    val status: StateFlow<ApiStatus> = _status.asStateFlow()

    private val _items = MutableStateFlow<List<MusicItem>>(emptyList())
    val items: StateFlow<List<MusicItem>> = _items.asStateFlow()

    private val _item = MutableStateFlow(MusicItem())
    val item: StateFlow<MusicItem> = _item.asStateFlow()

    val searchQuery = MutableStateFlow("")

    init {
        viewModelScope.launch {
            searchQuery.debounce(500).collect { doSearch() }
        }
    }

    private fun doSearch() {
        if (searchQuery.value.length > 3) {
            _status.value = ApiStatus.LOADING
            viewModelScope.launch {
                when (val result = DataHelper.search(searchQuery.value)) {
                    is Result.Success -> {
                        _items.value = result.value
                        _status.value = ApiStatus.DONE
                    }
                    is Result.Failure -> {
                        _items.value = listOf()
                        _status.value = ApiStatus.ERROR
                    }
                }
            }
        }
    }

    fun bindItem(id: Int) {
        _item.value = _items.value.find { it.id == id } ?: MusicItem()
    }

    override fun onQueryTextSubmit(query: String?): Boolean = false

    override fun onQueryTextChange(newText: String): Boolean {
        searchQuery.value = newText
        return true
    }

}
