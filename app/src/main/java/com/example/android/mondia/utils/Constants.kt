package com.example.android.mondia.utils


const val QUERY_NAME = "query"
const val QUERY_INCLUDE_ARTIST = "includeArtists"

const val HEADER_CONTENT_TYPE_KEY = "Content-Type"
const val HEADER_CONTENT_TYPE_VALUE = "application/x-www-form-urlencoded"

const val HEADER_GATEWAY_KEY = "X-MM-GATEWAY-KEY"

const val HEADER_ACCEPT_KEY = "Accept"
const val HEADER_ACCEPT_VALUE = "application/json"
const val HEADER_AUTH_KEY = "Authorization"



