package com.example.android.mondia

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.mondia.data.ApiStatus
import com.example.android.mondia.data.MusicItem
import com.example.android.mondia.view.MusicItemAdapter
import kotlinx.coroutines.*
import java.io.IOException
import java.net.URL


@BindingAdapter("imageUrl")
fun loadImageFromUrl(imageView: ImageView, url: String?) {
    val urlImage = URL("http:$url")
    imageView.setImageResource(R.drawable.loading_animation)
    val result: Deferred<Bitmap?> = GlobalScope.async {
        try {
            BitmapFactory.decodeStream(urlImage.openStream())
        } catch (e: IOException) {
            imageView.setImageResource(R.drawable.ic_broken_image)
            null
        }
    }

    GlobalScope.launch(Dispatchers.Main) {
        // show bitmap on image view when available
        val bitmap = result.await()
        if (bitmap != null)
            imageView.setImageBitmap(result.await())
    }

}

@BindingAdapter("listData")
fun bindRecyclerView(
    recyclerView: RecyclerView,
    data: List<MusicItem>?
) {
    val adapter = recyclerView.adapter as MusicItemAdapter
    adapter.submitList(data)

}

@BindingAdapter("apiStatus")
fun bindStatus(statusImageView: ImageView, status: ApiStatus?) {
    when (status) {
        ApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        ApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        ApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}
