package com.example.android.mondia.view.musiclist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.android.mondia.R
import com.example.android.mondia.data.ApiStatus
import com.example.android.mondia.databinding.FragmentMainBinding
import com.example.android.mondia.utils.hideKeyboard
import com.example.android.mondia.utils.showToast
import com.example.android.mondia.view.MusicItemAdapter
import com.example.android.mondia.view.MusicSharedViewModel
import kotlinx.coroutines.flow.collect

class MusicListFragment : Fragment() {

    private lateinit var sharedViewModel: MusicSharedViewModel

    private val binding get() = _binding!!
    private var _binding: FragmentMainBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            sharedViewModel =
                ViewModelProvider(requireActivity()).get(MusicSharedViewModel::class.java)
                    .also { viewModel = it }

            lifecycleOwner = this@MusicListFragment
        }

        val adapter = MusicItemAdapter {
            val action = MusicListFragmentDirections.actionMusicListFragmentToDetailsFragment(it.id)
            view.findNavController().navigate(action)
        }
        setupRecyclerView(adapter)
        initObservers()
    }

    private fun initObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            sharedViewModel.status.collect {
                hideKeyboard()
                if (it == ApiStatus.ERROR)
                    showToast(getString(R.string.error_getting_results))
            }
        }
    }

    private fun setupRecyclerView(musicItemAdapter: MusicItemAdapter) {
        binding.rvItems.apply {
            adapter = musicItemAdapter
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }

}
