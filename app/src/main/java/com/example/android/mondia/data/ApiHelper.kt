package com.example.android.mondia.data

import com.example.android.mondia.BuildConfig
import com.example.android.mondia.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

object ApiHelper {
    private const val BASE_URL = "http://staging-gateway.mondiamedia.com"
    private const val SEARCH_URL = "/v2/api/sayt/flat"
    private const val TOKEN_URL = "/v0/api/gateway/token/client"

    enum class RequestMethod {
        GET, POST, PUT, DELETE
    }

    suspend fun search(token: String, query: String) = sendRequest(
        requestUrl = "$BASE_URL$SEARCH_URL",
        query = mapOf(QUERY_NAME to query, QUERY_INCLUDE_ARTIST to false.toString()),
        headers = mapOf(HEADER_AUTH_KEY to "Bearer $token")
    )

    suspend fun getToken() = sendRequest(
        requestType = RequestMethod.POST, requestUrl = "$BASE_URL$TOKEN_URL", headers = mapOf(
            HEADER_CONTENT_TYPE_KEY to HEADER_CONTENT_TYPE_VALUE,
            HEADER_GATEWAY_KEY to BuildConfig.GATEWAY_KEY,
            HEADER_ACCEPT_KEY to HEADER_ACCEPT_VALUE
        )
    )

    private suspend fun sendRequest(
        requestType: RequestMethod = RequestMethod.GET,
        requestUrl: String,
        headers: Map<String, String> = mapOf(HEADER_ACCEPT_KEY to HEADER_ACCEPT_VALUE),
        query: Map<String, String> = mapOf()
    ): Result<String> {

        return withContext(Dispatchers.IO) {
            Result {
                suspendCancellableCoroutine { continuation ->
                    val url = URL("$requestUrl?${query.getParametersString()}")

                    (url.openConnection() as HttpURLConnection).run {
                        requestMethod = requestType.name
                        headers.forEach {
                            setRequestProperty(it.key, it.value)
                        }

                        doInput = true
                        doOutput = requestType == RequestMethod.POST

                        if (continuation.isActive) {
                            when (responseCode) {
                                HttpURLConnection.HTTP_OK -> continuation.resume(inputStream.bufferedReader().readText())
                                HttpURLConnection.HTTP_UNAUTHORIZED -> continuation.resumeWithException(UnAuthorizedException)
                                else -> continuation.resumeWithException(Exception())
                            }
                        }
                    }
                }
            }
        }
    }
}

object UnAuthorizedException : Exception()

private fun Map<String, String>.getParametersString(): String {
    return map { entry -> "${entry.key}=${entry.value}" }
        .joinToString("&")
}

