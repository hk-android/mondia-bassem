package com.example.android.mondia.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.mondia.data.MusicItem
import com.example.android.mondia.databinding.MusicItemBinding

class MusicItemAdapter(private val onItemClicked: (MusicItem) -> Unit) :
    ListAdapter<MusicItem, MusicItemAdapter.MusicItemViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicItemViewHolder {
        return MusicItemViewHolder(
            MusicItemBinding.inflate(
                LayoutInflater.from(parent.context)
            )
        )
    }

    override fun onBindViewHolder(holder: MusicItemViewHolder, position: Int) {
        with(getItem(position))
        {
            holder.itemView.setOnClickListener {
                onItemClicked(this)
            }
            holder.bind(this)
        }
    }


    class MusicItemViewHolder(private var binding: MusicItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MusicItem) {
            binding.item = item
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<MusicItem>() {
        override fun areItemsTheSame(oldItem: MusicItem, newItem: MusicItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MusicItem, newItem: MusicItem): Boolean {
            return oldItem.title == newItem.title
        }
    }

}